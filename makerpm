#!/bin/bash
#
#   makepkg - make packages compatible for use with pacman
#
#   Copyright (c) 2023 Noveria Network <noveria@noveria.org>
#   Copyright (c) 2006-2022 Pacman Development Team <pacman-dev@lists.archlinux.org>
#   Copyright (c) 2002-2006 by Judd Vinet <jvinet@zeroflux.org>
#   Copyright (c) 2005 by Aurelien Foret <orelien@chez.com>
#   Copyright (c) 2006 by Miklos Vajna <vmiklos@frugalware.org>
#   Copyright (c) 2005 by Christian Hamar <krics@linuxforum.hu>
#   Copyright (c) 2006 by Alex Smith <alex@alex-smith.me.uk>
#   Copyright (c) 2006 by Andras Voroskoi <voroskoi@frugalware.org>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

declare -r makerpm_version='0.1.0'
declare -r BUILDSCRIPT='SPECBUILD'
declare -r topdir="$PWD"
declare -r rpmbuild="rpmbuild --define \"_topdir $topdir\""
declare arch="noarch"

LIBRARY=${LIBRARY:-'/usr/lib/makerpm/util'}

# Options
CLEANBUILD=false
CLEANUP=false
NODEPS=false
NOBUILD=false
SOURCEONLY=false
NOARCHIVE=false
NOCHECK=false
NOPREP=false
NOEXTRACT=false
FORCE=false
INSTALL=false

# Importing utils
for lib in "$LIBRARY"/*.sh; do
	source "$lib"
done

usage() {
	printf "makerpm (rpmbuild) %s\n" "$makerpm_version"
	echo
	printf -- "$(gettext "Make packages compatible for use with rpm/dnf")\n"
	echo
	printf -- "$(gettext "Usage: %s [options]")\n" "$0"
	echo
	printf -- "$(gettext "Options:")\n"
	printf -- "$(gettext "  -c, --clean      Clean up work files after build")\n" # rpmbuild --rmsource --clean
	printf -- "$(gettext "  -C, --cleanbuild Truncate %s dir before building the package")\n" "SOURCES" # rpmbuild --rmsource
    printf -- "$(gettext "  -i, --install    Install the finished binary after successful building")\n" # rpmbuild --install
	printf -- "$(gettext "  -d, --nodeps     Skip all dependency checks")\n" # rpmbuild --nodeps
    printf -- "$(gettext "  -e, --noextract  Do not extract source files (use existing %s dir)")\n" "\$srcdir/"
    printf -- "$(gettext "  -f, --force      Overwrite existing package")\n"
	printf -- "$(gettext "  -h, --help       Show this help message and exit")\n"
	printf -- "$(gettext "  -o, --nobuild    Download and extract files only")\n" # rpmbuild -bf --nobuild
	printf -- "$(gettext "  -p <file>        Use an alternate build script (instead of '%s')")\n" "$BUILDSCRIPT"
	printf -- "$(gettext "  -V, --version    Show version information and exit")\n"
	printf -- "$(gettext "  --allsource      Generate a source-only tarball including downloaded sources")\n" # rpmbuild -bs
	printf -- "$(gettext "  --noarchive      Do not create package archive")\n" # rpmbuild -bc
    printf -- "$(gettext "  --nocheck        Do not run the %s function in the %s")\n" "%check" "$BUILDSCRIPT" # rpmbuild --nocheck
	printf -- "$(gettext "  --noprepare      Do not run the %s function in the %s")\n" "%prep" "$BUILDSCRIPT" # rpmbuild --noprep
	echo
	printf -- "$(gettext "If %s is not specified, %s will look for '%s'")\n" "-p" "makerpm" "$BUILDSCRIPT"
	echo
}

version() {
	printf "makerpm (rpmbuild) %s\n" "$makerpm_version"
    printf -- "Copyright (c) 2023      Noveria Network <noveria@noveria.org>.\n"
	printf -- "Copyright (c) 2006-2022 Pacman Development Team <pacman-dev@lists.archlinux.org>.\n"
	printf -- "Copyright (C) 2002-2006 Judd Vinet <jvinet@zeroflux.org>.\n"
	printf '\n'
	printf -- "$(gettext "\
This is free software; see the source for copying conditions.\n\
There is NO WARRANTY, to the extent permitted by law.\n")"
}

move_binary() {
    find . -name *.rpm -execdir mv '{}' $PWD ';'
    rm -rf $PWD/{RPMS,SRPMS}
}

cleanup() {
    msg "$(gettext "Cleaning up...")"
    rm -rf $PWD/{BUILD,BUILDROOT,SOURCES}
}

##
# START PROGRAM
##

umask 022

ARGLIST=("$@")

OPT_SHORT="cCidefhop:V"
OPT_LONG=("clean" "cleanbuild" "install" "noextract" "nodeps" "force" "help" "nobuild" "version" "allsource" "noarchive" "nocheck" "noprepare")

if ! parseopts "$OPT_SHORT" "${OPT_LONG[@]}" -- "$@"; then
    exit $E_INVALID_OPTION
fi
set -- "${OPTRET[@]}"
unset OPT_SHORT OPT_LONG OPTRET

while true; do
    case "$1" in
        -c|--clean)         CLEANUP=true;;
        -C|--cleanbuild)    CLEANBUILD=true;;
        -i|--install)       INSTALL=true;;
        -d|--nodeps)        NODEPS=true;;
        -e|--noextract)     NOEXTRACT=true;;
        -f|--force)         FORCE=true;;
        -h|--help)          usage; exit $E_OK;;
        -o|--nobuild)       NOBUILD=true;;
        -p)                 shift; BUILDFILE=$1;;
        -V|--version)       version; exit $E_OK;;
        --allsource)        SOURCEONLY=true;;
        --noarchive)        NOARCHIVE=true;;
        --nocheck)          NOCHECK=true;;
        --noprepare)        NOPREP=true;;

        --) shift; break;;
    esac
    shift
done

if [[ ! -w $PWD ]]; then
    plainerr "$(gettext "Aborting...")"
    exit $E_FS_PERMISSIONS
fi

BUILDFILE=${BUILDFILE:-$BUILDSCRIPT}
if [[ ! -f $BUILDFILE ]]; then
    error "$(gettext "%s does not exist.")" "$BUILDFILE"
    exit $E_PKGBUILD_ERROR
fi

declare -r pkgname="$(grep "Name:" $BUILDFILE | cut -d: -f2 | awk '{$1=$1};1')"
declare -r pkgver="$(grep "Version:" $BUILDFILE | cut -d: -f2 | awk '{$1=$1};1')"

if [[ -n "$(grep "BuildArch" SPECBUILD | cut -d: -f2 | awk '{$1=$1};1')" ]]; then
    msg "$(gettext "Updating BuildArch.")"
    arch="$(grep "BuildArch" SPECBUILD | cut -d: -f2 | awk '{$1=$1};1')"
fi

if $NOEXTRACT && [[ -z $(find $PWD/SOURCES -name *.tar.gz) ]]; then
    error "$(gettext "SOURCE dir can not be empty.")"
    exit $E_MISSING_FILE
fi

if ! $NOEXTRACT && [[ ! -d $PWD/SOURCES ]]; then
    msg "$(gettext "Downloading sources.")"
    if [ ! [ $(grep "Source0:" $BUILDFILE | cut -d: -f2- | awk '{$1=$1};1') == *"git"* ]]; then
        spectool --define "_topdir $topdir" -g -R $BUILDFILE &> /dev/null
    fi
elif $NOEXTRACT && [[ ! -z $(find $PWD/SOURCES -name *.tar.gz) ]]; then
    msg "$(gettext "Using already available sources.")"
elif ! $NOEXTRACT && [[ ! -z $(find $PWD/SOURCES -name *.tar.gz) ]]; then
    find $PWD/SOURCES -name $pkgname* -execdir rm '{}' ';'
    msg "$(gettext "Re-downloading sources.")"
    if [ ! [ $(grep "Source0:" $BUILDFILE | cut -d: -f2- | awk '{$1=$1};1') == *"git"* ]]; then
        spectool --define "_topdir $topdir" -g -R $BUILDFILE &> /dev/null
    fi
fi

if $SOURCEONLY; then
    eval $rpmbuild -bs $BUILDFILE
    exit $E_OK
fi

if $NODEPS; then
    warning "$(gettext "Skipping dependency checks.")"
fi

if $NOBUILD; then
    eval $rpmbuild -bf --nobuild $BUILDFILE
    msg "$(gettext "Sources are ready.")"
    exit $E_OK
fi

if $NOARCHIVE; then
    eval $rpmbuild -bc $BUILDFILE
    msg "$(gettext "Package directory is ready.")"
    exit $E_OK
fi

if $NOPREP; then
    eval $rpmbuild --noprep $BUILDFILE
    exit $E_OK
fi

if [[ !$NODEPS && !$NOBUILD && !$SOURCEONLY && !$NOARCHIVE && !$NOPREP ]]; then
    
    if [[ ! -z $(find $PWD -name *.rpm) ]]; then
        if ! $FORCE; then
            error "$(gettext "Package has already been built.")"
            exit $E_ALREADY_BUILT
        fi
        find $PWD -name $pkgname*.rpm -execdir rm '{}' ';'
    fi

    if [[ $NODEPS == false ]]; then
        if [[ -n $(grep "BuildRequires:" $BUILDFILE | cut -d: -f2 | awk '{$1=$1};1') ]]; then
            msg2 "Installing build-dependencies"
            sudo dnf install $(grep "BuildRequires:" $BUILDFILE | cut -d: -f2 | awk '{$1=$1};1') -y
            if [[ $? -ne 0 ]]; then
                error "Could not install all build-dependencies!"
                exit 1
            fi
        fi
    fi
    
    eval $rpmbuild --quiet -bb $($CLEANUP && echo "--rmsource" || echo "--noclean") $($CLEANBUILD && echo "--rmsource") $($NOCHECK && echo "--nocheck") $BUILDFILE
    move_binary
    $CLEANUP && cleanup
    msg "$(gettext "Finished making: %s")" "$(basename $(find . -name *.$arch.rpm))"
    
    if $INSTALL; then
        msg "$(gettext "Installing finished package on system: %s")" "$(basename $(find . -name $pkgname*.rpm))"
        sudo dnf install $(find . -name $pkgname*.rpm) -y
    fi
    
fi
