rpmbuild: sources
	git pull
	rpmbuild --define "_topdir $(PWD)" -bb SPECBUILD
	find . -name *.rpm -exec mv {} . \;

sources: dependencies
	spectool --define "_topdir $(PWD)" -g -R SPECBUILD

dependencies:
	sudo dnf install rpmdevtools rpm-build -y

.PHONY = install
install:
	sudo dnf install ./$(shell basename $(shell find . -name makerpm*.rpm)) -y

.PHONY = uninstall
uninstall:
	sudo dnf remove makerpm -y

.PHONY = clean
clean:
	rm -rf BUILD/ BUILDROOT/ RPMS/ SOURCES/ SRPMS/ *.rpm
