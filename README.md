# makerpm

`makerpm` is a wrapper for `rpmbuild` which builds `.rpm` packages for RHEL/Fedora/CentOS.

The codebase has been heavily inspired (and some parts even copied) by [ArchLinux's `makepkg`](https://gitlab.archlinux.org/pacman/pacman/blob/master/scripts/makepkg.sh.in) for it's AUR-Packages.

The format of the `SPECFILE` is a simple rpm-spec file, as described [here](https://developer.fedoraproject.org/deployment/rpm/about.html).

makerpm builds the package in the same directory where the `SPECFILE` lies, which removes the pain, from building it in the home-directory of the user.

When no arguments passed, the build-directory will remain as is. When the `-c` parameter is passed, it will clean the complete directory and leaves you only the finished `.rpm` package.

For more information, type `makerpm -h`